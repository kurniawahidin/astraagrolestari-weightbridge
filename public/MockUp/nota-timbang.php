<link rel="stylesheet" href="./dist/css/adminlte.min.css">

<style>
	.header {
		margin-bottom: 40px;
	}

	.header .judul {
		font-size: 20px;
		text-align: center;
	}

	.wb-data {
		border-radius: 8px;
		border: solid 1px black;
		margin: 20px 0;
		padding: 10px;
	}
</style>
<div class="nota-timbang">
	<div class="header">
		<div class="judul">NOTA TIMBANG</div>
		<div class="judul">PT. SAWIT ASAHAN INDAH 1-SAI</div>
	</div>
	<div class="body row">
		<div class="col-6 row">
			<div class="label col-4">Nomor Chit</div>
			<div class="isi col-8">: SAI1P1210901001</div>
			<div class="label col-4">Customer Code</div>
			<div class="isi col-8">: SAI 1</div>
			<div class="label col-4">Transporter Code</div>
			<div class="isi col-8">: PT. TRI BHAKTI PRIMA PERKASA</div>
			<div class="label col-4">Code UNIT</div>
			<div class="isi col-8">: XSAIDT001 - BK 2223 DD</div>
			<div class="label col-4">Product Code</div>
			<div class="isi col-8">: 400012100 - Tanda Buah Segar</div>
			<div class="label col-4">Transaction Type</div>
			<div class="isi col-8">: TBS Internal</div>
			<div class="label col-4">Driver</div>
			<div class="isi col-8">: ARION - 9231245</div>
			<div class="label col-4">Helper 1</div>
			<div class="isi col-8">: BEDU - 3234325</div>
			<div class="label col-4">Helper 2</div>
			<div class="isi col-8">: SAIPUL - 3234325</div>
			<div class="label col-4">SABNO</div>
			<div class="isi col-8">: TTBSAI121012768</div>
		</div>
		<div class="col-6">
			<div class="wb-data">
				<h5>WB - IN</h5>
				<div class="row">
					<div class="col-4">Date / Time</div>
					<div class="col-8">: 21-09-2021 21:34:44</div>
					<div class="col-4">Weight</div>
					<div class="col-8">: 15.000</div>
				</div>
			</div>
			<div class="wb-data">
				<h5>WB - OUT</h5>
				<div class="row">
					<div class="col-4">Date / Time</div>
					<div class="col-8">: 21-09-2021 22:04:44</div>
					<div class="col-4">Weight</div>
					<div class="col-8">: 8.000</div>
				</div>
			</div>
			<div class="row">
				<div class="col-4">NETTO<sub>Kg</sub></div>
				<div class="col-8">: 8.000</div>
			</div>
		</div>
	</div>
</div>