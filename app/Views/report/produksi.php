<?php
use CodeIgniter\View\View;

/**
 * @var View $this
 */

$this->title = 'Report Produksi TBS';
$this->breadcrumbs = [$this->title];
?>
<?= $this->extend('layouts/main') ?>
<?=$this->section('on-header')?>
<link rel="stylesheet" type="text/css" href="/assets/datatables.css"/>
<link rel="stylesheet" type="text/css" href="/MockUp/plugins/jquery-ui/jquery-ui.css"/>
<style>
	.dt-buttons {
		margin: 3px 0;
	}
	table.table-filter td {
		padding: 5px 10px;
	}
</style>
<?=$this->endSection()?>

<?=$this->section('content')?>
<section class="content">
	<div class="container-fluid">
		<div class="produksi-tbs-filter">
			<table class="table-filter">
				<tr>
					<td>
						<label>Tanggal</label>
					</td>
					<td>
						<input type="text" name="dateFrom" id="filter-DateFrom" value="<?= date('Y-m-d', strtotime('-14 days')) ?>" style="width: 100px;">
						s/d
						<input type="text" name="dateTo" id="filter-DateTo" value="<?= date('Y-m-d') ?>" style="width: 100px;">
					</td>
					<td rowspan="2" style="vertical-align: bottom;">
						<button class="btn btn-primary" onclick="dataTableReload()">Show Data</button>
					</td>
				</tr>
				<tr>
					<td>
						<label>Afdeling</label>
					</td>
					<td>
						<input type="text" name="afdeling" id="filter-Afdeling" style="width: 100%;">
					</td>
				</tr>
			</table>
		</div>
		<div class="produksi-tbs-page">
			<table id="produksiTBS-table" class="table table-bordered">
				<thead class="bg-success">
					<tr>
						<th>TANGGAL</th>
						<th>CHIT NUMBER</th>
						<th>AFDELING</th>
						<th>BLOK</th>
						<th>JANJANG</th>
					</tr>
				</thead>
				<tbody></tbody>
				<tfoot>
					<tr>
						<th colspan="3" class="text-center">Total</th>
						<th id="count-block"></th>
						<th id="sum-janjang"></th>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</section>
<?=$this->endSection()?>

<?=$this->section('end-body')?>
<script src="/assets/DataTables-1.11.3/js/jquery.dataTables.min.js"></script>
<script src="/assets/datatables.min.js"></script>
<script src="/MockUp/plugins/jquery-ui/jquery-ui.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js"></script>
<script type="text/javascript">
var tbProduksi;
$(document).ready(function(){
	$('#filter-DateFrom').datepicker({dateFormat: 'yy-mm-dd'});
	$('#filter-DateTo').datepicker({dateFormat: 'yy-mm-dd'});

	$('table#produksiTBS-table').on('xhr.dt', function(e, settings, json, xhr){
		$('#count-block').text(json.recordsFiltered);
		$('#sum-janjang').text(json.sum_janjang);
	});

	tbProduksi = $('table#produksiTBS-table').DataTable({
		serverSide: true,
		ajax: {
			url: '/report-production',
			type: 'post',
			data: function(data) {
				data.dateFrom = $('#filter-DateFrom').val();
				data.dateTo = $('#filter-DateTo').val();
				data.afdeling = $('#filter-Afdeling').val();
			},
		},
		processing: true,
		order: [],
		lengthMenu: [
			[25, 50, 100],
			[25, 50, 100],
		],
		columns: [
			{data: 'tanggal'},
			{data: 'chitnumber'},
			{data: 'afdeling'},
			{data: 'block'},
			{data: 'janjang'},
		],
		columnDefs: [
			{
				targets: [],
				orderable: false,
			}
		],
		dom: "<'row'<'col-sm-12'B><'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-5'i><'col-sm-7'p>>",
		buttons: [
			{
				extend: 'csv',
				className: 'btn btn-secondary btn-sm',
			},
			{
				extend: 'excel',
				className: 'btn btn-secondary btn-sm',
			},
			{
				extend: 'pdf',
				className: 'btn btn-secondary btn-sm',
			},
		],
	});
});

function dataTableReload()
{
	tbProduksi.ajax.reload();
}
</script>
<?=$this->endSection()?>