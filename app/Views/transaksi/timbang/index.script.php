<?php

use App\Controllers\Timbang;
use App\Models\ParameterValueModel;

$adjustWeight = (ParameterValueModel::getValue('ADJUSTWEIGHT') == 'Y');
?>
<script>
var tbs_external;
var reloadTransType = true;

$(document).ready(function() {
	if ('<?= ($tr_wb['kab_type'] ?? '') ?>' == 'EXTERNAL') {
		tbs_external = false;
		setTbsExternal();
	} else {
		tbs_external = true;
		setNonTbsExternal();
	}

	cekTimbangan();

	$('select#timbang-ProductCode').change(function(){
		if (reloadTransType) {
			var produkCode = $('select#timbang-ProductCode').val();
			if (produkCode == '<?= Timbang::produkTBS['value'] ?>')	{
				$('#grading-button').removeAttr('disabled');
				$('#kualitas-button').attr('disabled', 'disabled');
			} else if ((produkCode == '<?= Timbang::produkCpoKode ?>') || (produkCode == '<?= Timbang::produkKernelKode ?>')) {
				$('#grading-button').attr('disabled', 'disabled');
				$('#kualitas-button').removeAttr('disabled');
			} else {
				$('#grading-button').removeAttr('disabled');
				$('#kualitas-button').removeAttr('disabled');
			}

			$.get('/timbang/transaction-code/' + produkCode, function(data){
				var transaksiSelect = $('select#timbang-TransactionType');
				transaksiSelect.empty();
				data.forEach(tr => {
					transaksiSelect.append($(
						'<option/>',
						{
							value: tr.transactioncode,
							text: tr.title
						}
					));
				});
				$('select#timbang-TransactionType').change();
			}).fail(function(jqXHR, textStatus, errorThrown){});
		}
	});
});

function setNonTbsExternal(){
	if (tbs_external) {
		html_form_wb = $('#form-wb').html();
		$('#form-container').html(html_form_wb);
	}
	tbs_external = false;
};

function setTbsExternal(){
	if (! tbs_external) {
		html_form_wb = $('#form-wb-external').html();
		$('#form-container').html(html_form_wb);
	}
	tbs_external = true;
};

function cekTimbangan() {
	$.ajax({
		url: '/timbang/weighRead',
		method: 'GET',
	}).done(function(msg) {
		//alert('Berat : ' + msg);
		$('#weight-current').val(msg.value);
		setTimeout(function() {
			cekTimbangan();
		}, (msg.value == '#Err' ? 5000 : <?= ParameterValueModel::getValue('WBBROWSERREFRESH') ?? '2000' ?>))
	}).fail(function() {
		$('#weight-current').val('#Er');
		setTimeout(function() {
			cekTimbangan();
		}, 5000)
	}).always(function() {
	});
};

function set_wb_in(){
	var weightCurrent = $('input#weight-current').val();
	var now = new Date();
	$('input#timbang-TimeIn').val(now.getFullYear() + '-' + ('0' + (now.getMonth() + 1)).slice(-2) + '-' + ('0' + now.getDate()).slice(-2) + ' ' + now.getHours() + ':' + now.getMinutes() + ':' + now.getSeconds());
	if (weightCurrent !== '#Err') {
		$('input#timbang-WeightIn').val(weightCurrent);
	}
}

function set_wb_out(){
	var weightCurrent = $('input#weight-current').val();
	var now = new Date();
	$('input#timbang-TimeOut').val(now.getFullYear() + '-' + ('0' + (now.getMonth() + 1)).slice(-2) + '-' + ('0' + now.getDate()).slice(-2) + ' ' + now.getHours() + ':' + now.getMinutes() + ':' + now.getSeconds());
	if (weightCurrent !== '#Err') {
		$('input#timbang-WeightOut').val(weightCurrent);
	
		var weight_in = $('input#timbang-WeightIn').val();
		weight_out = parseInt(weightCurrent.replace('.', ''));
		weight_in = parseInt(weight_in.replace('.', ''));
		netto = Math.abs(weight_out - weight_in);
		$('input#timbang-Netto').val(netto);
	}
}

$('input#timbang-WeightOut').on('input', function(){
	var weight_out = $('input#timbang-WeightOut').val();
	var weight_in = $('input#timbang-WeightIn').val();
	weight_out = parseInt(weight_out.replace('.', ''));
	weight_in = parseInt(weight_in.replace('.', ''));
	netto = Math.abs(weight_out - weight_in);
	$('input#timbang-Netto').val(netto);
});

$('#get-NFC-button').on('click', function() {
	reloadTransType = false;
	var $this = $(this);
	var $html = $this.html();
	//$this.prop('disabled', true);
	$this.hide();
	$('#nfcCancel-button').show();
	$this.html($this.data('loading-text'));
	$this.prop('data-loading-text', $html);
	$.ajax({
		url: '/timbang/nfcRead',
		method: 'GET',
	}).done(function(msg, textStatus, jqXHR){
		/** Log responseText */
		$('#nfc-data').append('<div class="console-text">' + jqXHR.responseText + '</div>');
		if (msg.tipe == 'KAB External') {
			setTbsExternal();
			if (msg.chitnumber != '') {
				if (confirm('ID KAB telah direkam sebelumnya, akan WB Out sekarang?')) {
					location.href =  '/timbang?id=' + msg.chitnumber;
				}
			}
			
			$('input#timbang-NomorTicket').val(msg.nomorticket);
			$('input#timbang-kab_type').val('EXTERNAL');
			if ($('select#timbang-ProductCode option[value="'+ msg.produk.value +'"]').length <= 0){/** Cek Kode Produk tidak ada dalam option */
				$('select#timbang-ProductCode').append($('<option/>', msg.produk));
			}
			$('select#timbang-ProductCode').val(msg.produk.value).change();

			if ($('select#timbang-TransactionType option[value="'+ msg.transaksi.value +'"]').length <= 0){/** Cek Kode Transaksi tidak ada dalam option */
				$('select#timbang-TransactionType').append($('<option/>', msg.transaksi));
			}
			$('select#timbang-TransactionType').val(msg.transaksi.value).change();

			$('input#timbang-GateIn').val(msg.gate_in);
			$('input#timbang-GateOut').val(msg.gate_out);
			$('input#timbang-BoardingIn').val(msg.boarding_in);
			$('input#timbang-JenisUnit').val(msg.jenis_unit);
			$('input#timbang-NomorPolisi').val(msg.nomor_polisi);
			$('input#timbang-NamaDriver').val(msg.nama_driver);
			$('input#timbang-KodeSupplier').val(msg.kode_supplier);
			$('input#timbang-NamaSupplier').val(msg.nama_supplier);
			$('input#timbang-WilayahAsalTBS').val(msg.wilayah_asal_tbs);
			$('input#timbang-Kab_Prop').val(msg.kab_prop);
			$('input#timbang-Kab_createdate').val(msg.kab_createdate);
			$('input#timbang-Kab_createby').val(msg.kab_createby);
			$('#timbang-kabraw').val(msg.kabraw);

		} else {
			setNonTbsExternal();
			var driverSelect = $('select#timbang-Driver');
			var helper1Select = $('select#timbang-Helper1');
			var helper2Select = $('select#timbang-Helper2');
			var unitSelect = $('select#timbang-CodeUnit');
			var transporterSelect = $('select#timbang-TransporterCode');
			var customerSelect = $('select#timbang-CustomerCode');
			var produkSelect = $('select#timbang-ProductCode');
			var transaksiSelect = $('select#timbang-TransactionType');
			var sitecodeSelect = $('select#timbang-SiteCode');
			var inputKabType = $('#timbang-kab_type')
			var kartuError = false;

			if (msg.tipe == 'SKU') { /** Cek is Employee ID Card */
				var employeId = msg.data.npk;
				var employeName = msg.data.name;

				var driver_id = driverSelect.val();
				if (driver_id == null || driver_id == '') {/** ID Driver masih kosong */
					if ($("select#timbang-Driver option[value='" + employeId + "']").length <= 0) { /** Cek ID Employe tidak ada dalam option */
						driverSelect.append($('<option/>', {
							value: employeId,
							text: employeName + ' - ' + employeId,
						}));					
					}
					driverSelect.val(employeId).change();
				} else { /** ID Driver telah ada, masukkan ke helper1 atau helper2 */
					var helper1_id = helper1Select.val();
					if (helper1_id == null || helper1_id == '') {/** ID Helper 1 masih kosong */
						if ($('select#timbang-Helper1 option[value="'+ employeId +'"]').length <= 0) { /** Cek ID Employee tidak ada di dalam option */
							helper1Select.append($('<option/>', {
								value: employeId,
								text: employeName + ' - ' + employeId,
							}));
						}
						helper1Select.val(employeId).change();
					} else {
						var helper2_id = helper2Select.val();
						if (helper2_id == null || helper2_id == '') {/** ID Helper 2 masih kosong */
							if ($('select#timbang-Helper2 option[value="'+ employeId +'"]').length <= 0) { /** Cek ID Employee tidak ada di dalam option */
								helper2Select.append($('<option/>', {
									value: employeId,
									text: employeName + ' - ' + employeId,
								}));
							}
							helper2Select.val(employeId).change();
						} else {
							alert('Driver, Helper1 dan Helper2 telah ada');
						}
					}
				}
			} else if (msg.tipe == 'ID_DT') {
				if ($('select#timbang-CodeUnit option[value="'+ msg.data.unit_id +'"]').length <= 0) {/** Cek ID Unit tidak ada dalam option */
					unitSelect.append($('<option/>', {
						value: msg.data.unit_id,
						text: msg.data.unit_id + ' - ' + msg.data.plat_no,
					}));
				}
				unitSelect.val(msg.data.unit_id).change();
				
				if ($('select#timbang-TransporterCode option[value="'+ msg.data.tran_id +'"]').length <= 0){/** Cek ID Transporter tidak ada dalam option */
					transporterSelect.append($('<option/>', {
						value: msg.data.tran_id,
						text: msg.data.tran_name,
					}));
				}
				transporterSelect.val(msg.data.tran_id).change();
			} else if (msg.tipe == 'KAB'){
				if (msg.chitnumber != '') {
					if (confirm('ID KAB telah direkam sebelumnya, akan WB Out sekarang?')) {
						location.href =  '/timbang?id=' + msg.chitnumber;
					}
				}
				inputKabType.val(msg.kab_type);
				$('#kabcode').val(msg.kabcode);

				$('#timbang-kabraw').val(msg.kabraw);
				if ($('select#timbang-CustomerCode option[value="'+ msg.customer.value +'"]').length <= 0){/** Cek ID Customer tidak ada dalam option */
					customerSelect.append($('<option/>', msg.customer));
				}
				customerSelect.val(msg.customer.value).change();

				if ($('select#timbang-ProductCode option[value="'+ msg.produk.value +'"]').length <= 0){/** Cek Kode Produk tidak ada dalam option */
					produkSelect.append($('<option/>', msg.produk));
				}
				produkSelect.val(msg.produk.value).change();

				if ($('select#timbang-TransactionType option[value="'+ msg.transaksi.value +'"]').length <= 0){/** Cek Kode Transaksi tidak ada dalam option */
					transaksiSelect.append($('<option/>', msg.transaksi));
				}
				transaksiSelect.val(msg.transaksi.value).change();

				if ($('select#timbang-SiteCode option[value="'+ msg.sitecode.value +'"]').length <= 0) { /** Cek kode site tidak ada dalam option */
					sitecodeSelect.append($('<option/>', msg.sitecode));
				}
				sitecodeSelect.val(msg.sitecode.value).change();

				var tbDataKABtbody = $('table#timbang-DataKAB tbody');
				tbDataKABtbody.empty();
				var totalJJG = 0;
				$.each(msg.noc, function(index, value){
					hidden = '<input name="noc['+ index +'][nocvalue]" value="'+ value.nocvalue +'" type="hidden" />';
					hidden += '<input name="noc['+ index +'][nocsite]" value="'+ value.nocsite +'" type="hidden" />';
					hidden += '<input name="noc['+ index +'][nocdate]" value="'+ value.nocdate +'" type="hidden" />';
					hidden += '<input name="noc['+ index +'][harvestdate]" value="'+ value.harvestdate +'" type="hidden" />';
					hidden += '<input name="noc['+ index +'][nocafd]" value="'+ value.nocafd +'" type="hidden" />';
					hidden += '<input name="noc['+ index +'][nocblock]" value="'+ value.nocblock +'" type="hidden" />';
					hidden += '<input name="noc['+ index +'][tgl_panen]" value="'+ value.tgl_panen +'" type="hidden" />';
					hidden += '<input name="noc['+ index +'][jjg]" value="'+ value.jjg +'" type="hidden" />';

					tr_string = '<tr><td>'+ (index + 1) +'</td><td>'+ value.nocafd +'</td><td>'+ value.nocblock +'</td><td>'+ value.jjg +'</td>'+ hidden +'</tr>';
					tbDataKABtbody.append(tr_string);
					totalJJG += value.jjg;
				});
				$('#total-jjg').html(totalJJG);

				if (typeof msg.transporter !== 'undefined'){
					if ($('select#timbang-TransporterCode option[value="'+ msg.transporter.value +'"]').length <= 0){/** Cek ID Transporter tidak ada dalam option */
						transporterSelect.append($('<option/>', msg.transporter));
					}
					transporterSelect.val(msg.transporter.value).change();
				}
				if (typeof msg.unit !== 'undefined'){
					if ($('select#timbang-CodeUnit option[value="'+ msg.unit.value +'"]').length <= 0) {/** Cek ID Unit tidak ada dalam option */
						unitSelect.append($('<option/>', msg.unit));
					}
					unitSelect.val(msg.unit.value).change();
				}
				if (typeof msg.driver !== 'undefined'){
					if ($("select#timbang-Driver option[value='" + msg.driver.value + "']").length <= 0) { /** Cek ID Employe tidak ada dalam option */
						driverSelect.append($('<option/>', msg.driver));					
					}
					driverSelect.val(msg.driver.value).change();
				}
				if (typeof msg.helper1 !== 'undefined'){
					if ($("select#timbang-Helper1 option[value='" + msg.helper1.value + "']").length <= 0) { /** Cek ID Employe tidak ada dalam option */
						helper1Select.append($('<option/>', msg.helper1));					
					}
					helper1Select.val(msg.helper1.value).change();
				}
				if (typeof msg.helper2 !== 'undefined'){
					if ($("select#timbang-Helper2 option[value='" + msg.helper2.value + "']").length <= 0) { /** Cek ID Employe tidak ada dalam option */
						helper2Select.append($('<option/>', msg.helper2));					
					}
					helper2Select.val(msg.helper2.value).change();
				}
			} else if (msg.tipe == 'Canceled') {
				alert('Read cancelled');
				kartuError = true;
			} else if (msg.tipe == 'Unknown') {
				alert('Kartu tidak dikenali.');
				kartuError = true;
			}

			setTimeout(function(){
				if ($('input#type_continous_tapping').prop('checked') 
				&& !kartuError
				&& (inputKabType.val() != 'BOARDING')
				&& (
					(driverSelect.val() == '') ||
					(helper1Select.val() == '') ||
					(helper2Select.val() == '') ||
					(unitSelect.val() == '') ||
					(transporterSelect.val() == '') ||
					(customerSelect.val() == '') ||
					(produkSelect.val() == '') || 
					(transaksiSelect.val() == '')
				)){
					$this.click();
				}
			}, 2000);

		}
	}).fail(function(msg){
		alert('NFC Reader disconected');
		//alert('Read NFC Fail : \n' + msg.responseJSON.message);
	}).always(function(){
		$this.data('loading-text', $this.html());
		$this.html($html);
		//$this.prop('disabled', false);
		$this.show();
		$('#nfcCancel-button').hide();
	});
});

$('#nfcCancel-button').on('click', function(){
	$.ajax({
		url: '/timbang/nfcCancel',
		method: 'GET'
	})
});

function timbang_save(btn){
	if(parseInt($('input#timbang-WeightIn').val().replace('.', '')) == 0){
		alert('Berat timbang masuk (Weight In) belum diisi');
		return;
	}
	var complete = parseInt($('input#timbang-WeightOut').val().replace('.', '')) > 0;
	if(($('input#timbang-NomorChit').val() != '') && !complete){
		alert('Berat timbang keluar (Weight Out) belum diisi');
		return;
	}

	//complete dan berhasil simpan:
		//KAB Boarding, write wb_in - wb_out, weight_in - weight_out
		//Kab Non Boarding, format KAB
		//Print Nota Timbang

	var produkCode = $('select#timbang-ProductCode').val();
	if (complete > 0) {
		//complete dan produk == TBS, harus isi grading
		if ((produkCode == '<?= Timbang::produkTBS['value'] ?>') && 
		($('#grading-JumlahSampling').val() == '')) {
			if (confirm('Apakah anda akan mengisi grading?')) {
				$('#grading_formModal').modal('show');
				return;
			}
				
		//complete dan produk == CPO atau Kernel, opsi isi quality
		} else if (((produkCode == '<?= Timbang::produkCpoKode ?>') || (produkCode == '<?= Timbang::produkKernelKode ?>')) && 
		($('#kualitas-FFA').val() == '' && $('#kualitas-Temperature'))) {
			if (confirm('Apakah anda akan mengisi kualitas?')) {
				$('#kualitas_formModal').modal('show');
				return;
			}

		}
	}

	//proses simpan
	btn = $(btn)
	btn.prop('disabled', true);
	var data = $('form#timbang-form').serialize();
	$.post(
		'/timbang/save',
		data
	).done(function(msg, textStatus, jqXHR){
		if (msg.status == 'success') {
			//Berhasil simpan
			$(document).Toasts('create', {
				class: 'bg-success',
				title: 'Success',
				//subtitle: msg.status,
				body: msg.messages,
				autohide: true,
				delay: 1000,
			});

			if (complete) {
				<?php /*
				if (msg.saveAPI) {
					$.get('/timbang/save-api/' + msg.chitnumber)
					.done(function(msg, textStatus, jqXHR){
						$(document).Toasts('create', {
							class: 'bg-success',
							title: 'Success Post API',
							//subtitle: msg.status,
							//body: msg.messages,
							autohide: true,
							delay: 1000,
						});
					})
					.fail(function(jqXHR, textStatus, errorThrown){
						$(document).Toasts('create', {
							class: 'bg-danger',
							title: 'Error Post API',
							body: jqXHR.responseText,
							autohide: true,
							delay: 3000,
						});
					})
					.always(function(){});
				}
				*/ ?>
				boarding_type = $('#timbang-kab_type').val();

				if (boarding_type != null && boarding_type != '') {					
					prepareWriteKAB(msg.chitnumber);
				} else {
					printNota(msg.chitnumber);
				}
			} else {
				setTimeout(function(){
					if (msg.new) {
						window.location.replace('/timbang');
					} else {
						window.location.href = '/pending';
					}
				}, 1500);
			}
		} else {
			$(document).Toasts('create', {
				class: 'bg-danger',
				title: 'Error',
				subtitle: msg.status,
				body: msg.messages,
				autohide: true,
				delay: 5000,
			});
		}
	}).fail(function(jqXHR, textStatus, errorThrown){
		alert(errorThrown);
	}).always(function(){
		btn.prop('disabled', false);
	});

	/*
	setInterval(function(){
		btn.prop('disabled', false);
	}, 3000);
	*/
}

function prepareWriteKAB(chitnumber){
	$('#writeKAB_formModal').modal('show');
	$('#write-chitnumber').val(chitnumber);
	$('#write-transaction-type').val($('#timbang-TransactionType').val());
	$('#write-kab_type').val($('#timbang-kab_type').val());
	$('#writeKAB_formModal .modal-footer .btn-success').prop('disabled', false);
}

function writeKAB(elementButton){
	btn = $(elementButton);
	btn.prop('disabled', true);
	chitnumber = $('#write-chitnumber').val()
	$.getJSON('/timbang/write-kab/' + chitnumber, function(data){
		if (data.status == 'success') {
			$(document).Toasts('create', {
				class: 'bg-success',
				title: 'Success',
				//subtitle: msg.status,
				body: data.messages,
				autohide: true,
				delay: 1000,
			});
			setTimeout(function(){
				//$('#writeKAB_formModal').modal('hide');
				printNota(chitnumber);
			}, 1200);
		} else {
			alert(data.messages);
			btn.prop('disabled', false);
		}
	}).fail(function(jqXHR, textStatus, errorThrown){
		alert(errorThrown.messages);
		btn.prop('disabled', false);
	});
}

function printNota(chitnumber, autoRedirect = true){
	var newWin = window.open('/nota/' + chitnumber, 'Print-Windows');
	//newWin.document.open('/nota/' + chitnumber);
	//newWin.print();
	newWin.document.close();
	setTimeout(function(){
		newWin.close();
		if (autoRedirect) {
			setTimeout(function(){
				window.location.href = '/pending';
			}, 1500);
		}
	}, 100);
}
</script>