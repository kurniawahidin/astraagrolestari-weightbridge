<div class="form-group row">
	<label for="timbang-TransporterCode" class="col-sm-3 col-form-label">Transporter Code</label>
	<div class="col-sm-9">
		<select name="transportercode" id="timbang-TransporterCode" class="form-control">
			<option></option>
			<?php foreach ($transporters as $tr) : ?>
				<option value="<?= $tr['transportercode'] ?>" <?= (($tr_wb['transportercode'] ?? '') == $tr['transportercode']) ? 'selected' : '' ?>><?= $tr['name'] ?></option>
			<?php endforeach; ?>
		</select>
	</div>
</div>
<div class="form-group row">
	<label for="timbang-CodeUnit" class="col-sm-3 col-form-label">Code Unit</label>
	<div class="col-sm-9">
		<select name="unitcode" id="timbang-CodeUnit" class="form-control">
			<option></option>
			<?php foreach ($units as $tr) : ?>
				<option value="<?= $tr['unitcode'] ?>" <?= (($tr_wb['unitcode'] ?? '') == $tr['unitcode']) ? 'selected' : '' ?>><?= $tr['unitcode'] . ' - ' . $tr['platenumber'] ?></option>
			<?php endforeach; ?>
		</select>
	</div>
</div>
<div class="form-group row">
	<label for="timbang-Driver" class="col-sm-3 col-form-label">Driver</label>
	<div class="col-sm-9">
		<select name="npk_driver" id="timbang-Driver" class="form-control">
			<option></option>
			<?php foreach ($employees as $emp) : ?>
				<option value="<?= $emp['npk'] ?>" <?= (($tr_wb['npk_driver'] ?? '') == $emp['npk']) ? 'selected' : '' ?>><?= $emp['name'] . ' - ' . $emp['npk'] ?></option>
			<?php endforeach; ?>
		</select>
	</div>
</div>
<div class="form-group row">
	<label for="timbang-Helper1" class="col-sm-3 col-form-label">Helper 1</label>
	<div class="col-sm-9">
		<select name="npk_helper1" id="timbang-Helper1" class="form-control">
			<option></option>
			<?php foreach ($employees as $emp) : ?>
				<option value="<?= $emp['npk'] ?>" <?= (($tr_wb['npk_helper1'] ?? '') == $emp['npk']) ? 'selected' : '' ?>><?= $emp['name'] . ' - ' . $emp['npk'] ?></option>
			<?php endforeach; ?>
		</select>
	</div>
</div>
<div class="form-group row">
	<label for="timbang-Helper2" class="col-sm-3 col-form-label">Helper 2</label>
	<div class="col-sm-9">
		<select name="npk_helper2" id="timbang-Helper2" class="form-control">
			<option></option>
			<?php foreach ($employees as $emp) : ?>
				<option value="<?= $emp['npk'] ?>" <?= (($tr_wb['npk_helper2'] ?? '') == $emp['npk']) ? 'selected' : '' ?>><?= $emp['name'] . ' - ' . $emp['npk'] ?></option>
			<?php endforeach; ?>
		</select>
	</div>
</div>
<div class="form-group row">
	<label for="timbang-SabNo" class="col-sm-3 col-form-label">SABNO</label>
	<div class="col-sm-9">
		<input type="text" name="sabno" value="<?= $tr_wb['sabno'] ?? '' ?>" id="timbang-SabNo" class="form-control" readonly>
	</div>
</div>