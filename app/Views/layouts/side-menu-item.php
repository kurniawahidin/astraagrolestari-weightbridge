<a href="{url}" class="nav-link {active}">
	<i class="nav-icon fas fa-{icon}"></i>
	<p>{label}</p>
</a>