<?php

use App\Models\MTransporterModel;
use CodeIgniter\HTTP\Request;
use CodeIgniter\View\View;

/**
 * @var View $this
 * @var MTransporterModel $model
 * @var Request $request
 */

$this->title = 'Transporter';
?>
<?=$this->extend('master/main_view')?>