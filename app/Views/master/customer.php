<?php

use App\Models\MCustomerModel;
use CodeIgniter\HTTP\Request;
use CodeIgniter\View\View;

/**
 * @var View $this
 * @var MCustomerModel $model
 * @var Request $request
 */

$this->title = 'Customer';
?>
<?=$this->extend('master/main_view')?>