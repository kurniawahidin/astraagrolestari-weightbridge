<?php

namespace App\Controllers;

use App\Models\MCustomerModel;
use App\Models\MEmployeeModel;
use App\Models\MProductTransMapModel;
use App\Models\MSiteModel;
use App\Models\MTransporterModel;
use App\Models\MUnitModel;
use App\Models\ParameterValueModel;
use App\Models\TrCpoKernelQualityModel;
use App\Models\TrGradingModel;
use App\Models\TrKabModel;
use App\Models\TrWbModel;
use Config\Database;
use ZipArchive;

class Timbang extends BaseController
{
	public const produkTBS = [
		'value' => '400012100', // Fixed
		'text' => 'Tandan Buah Segar', //ambil name produk berdasar value
	];

	public const produkCpoKode = '501000001';

	public const produkKernelKode = '501010001';

	public const transaksiTBSInternal = [
		'value' => 'TBS Internal', // Fixed
		'text' => 'TBS INTI AAL', //ambil description berdasar value
	];

	public const transaksiTBSTitipOlah = [
		'value' => 'TBS Titip Olah',
		'text' => 'TBS Titip Olah dari sesama Grup AAL',
	];

	public const transaksiTBSExternal = [
		'value' => 'TBS External',
		'text' => 'TBS EXTERNAL AAL',
	];

	public function nfcCancel()
	{
		$device = ParameterValueModel::getValue('READERCONFIG');
		exec("echo 'STOP' > " . $device);
		return $this->response->setJSON(['succes' => true]);
	}

	public function nfcRead()
	{
		$rawdata = $this->getRawNFCData();

		$data = explode(';', $rawdata);

		if (count($data) > 2) {
			if ($data[2] == 'ID_DT') {
				$idDT = $this->getDT($data);
				return $this->response->setJSON($idDT);
			} elseif ($data[2] == 'SKU') {
				$sku = $this->getSKU($data);
				return $this->response->setJSON($sku);
			} elseif ((count($data) >= 6) && ($data[6] == 4)) {
				$bklReturn = $this->getKAB($data, $rawdata);
				return $this->response->setJSON($bklReturn);
			} elseif ($data[2] == 'TBS External') {
				//return $this->response->setJSON($data);
				return $this->response->setJSON($this->getKABExternal($data, $rawdata));
			}
		} else if ($rawdata == "OK\r\n") {
			return $this->response->setJSON([
				'tipe' => 'Canceled',
			]);
		}

		return $this->response->setJSON([
			'tipe' => 'Unknown',
			'rawdata' => $rawdata,
		]);
	}

	protected function getRawNFCData()
	{
		//set_time_limit(20);
		//$device = '/dev/ttyUSB0';
		$device = ParameterValueModel::getValue('READERCONFIG');
		$flowCtl = 'min 1 time 5 ignbrk -brkint -icrnl -imaxbel -opost -onlcr -isig -icanon -iexten -echo -echoe -echok -echoctl -echoke';

		$fd = fopen($device, 'rb+');
		exec('stty -F ' . $device . ' ' . $flowCtl);

		$welcome = fgets($fd);

		$command = 'READ';
		$hasil2 = fwrite($fd, $command);

		if (($confirm = fgets($fd)) == "OK\r\n") {
			session_write_close();
			//$rawdata = fgets($fd, 2400);			
			
			$rawdata = '';
			do {
				$new_rawdata = fgets($fd, 2400);
				$new_rawdata = trim($new_rawdata);
				$rawdata .= $new_rawdata;
				error_log("'$new_rawdata'");
			} while ((substr($new_rawdata, -3) != '#*E') && ($new_rawdata != 'OK') && ($new_rawdata != '== AAL Card Reader ==') && ($new_rawdata != ''));
			
			fwrite($fd, 'STOP');
		} else {
			$rawdata = null;
		}

		fclose($fd);

		return $rawdata;
	}

	protected function getDT(array $data)
	{
		$tipe = 'ID_DT';

		$unitTransporter = $this->getUnitTransporter($data[4], $data[6]);

		$data = [
			'unit_id' => $data[4],
			'plat_no' => $unitTransporter['unit']['text'],
			'tran_id' => $data[6],
			'tran_name' => $unitTransporter['transporter']['text'],
		];
		return [
			'tipe' => $tipe,
			'data' => $data,
		];
	}

	private function getUnitTransporter($unitcode, $transportercode = null)
	{
		$unitModel = new MUnitModel();
		if (($unit = $unitModel->find($unitcode)) == null) {
			$unit = [
				'unitcode' => $unitcode,
				'transportercode' => $transportercode,
				'platenumber' => '[New Record - ' . $unitcode . ']',
				'active' => 'Y',
			];
			$unitModel->insert($unit);
		}
		$transportercode = $unit['transportercode'];

		if ($transportercode != null) {
			$transporterModel = new MTransporterModel();
			if (($transporter = $transporterModel->find($transportercode)) == null) {
				$transporter = [
					'transportercode' => $transportercode,
					'name' => '[New Record - ' . $transportercode . ']',
					'transportertype' => 0,
					'active' => 'Y',
				];
				$transporterModel->insert($transporter);
			}
		} else {
			$transporter = [
				'transportercode' => null,
				'name' => null,
			];
		}

		return [
			'unit' => [
				'value' => $unit['unitcode'],
				'text' => $unit['platenumber'],
			],
			'transporter' => [
				'value' => $transporter['transportercode'],
				'text' => $transporter['name'],
			],
		];
	}

	private function getEmployee($npk, $name = null)
	{
		$employeeModel = new MEmployeeModel();
		if (($employee = $employeeModel->find($npk)) == null) {
			$employee = [
				'npk' => $npk,
				'name' => $name ?? ('[New Record - ' . $npk . ']'),
				'emp_type' => 'DRIVER / HELPER',
				'active' => 'Y',
			];
			$employeeModel->insert($employee);
		}
		return $employee;
	}

	protected function getSKU(array $data)
	{
		$tipe = 'SKU';
		$employee = $this->getEmployee(str_pad($data[3], 7, '0', STR_PAD_LEFT), $data[4]);
		return [
			'tipe' => $tipe,
			'data' => $employee,
		];
	}

	protected function getKAB(array $data, $rawdata)
	{
		//$kabraw = $data[3];

		$noc_decoded = base64_decode($data[3]);

		$rawnoc = $this->getRawNOC($noc_decoded);

		$count_noc = count($rawnoc);

		$noc = [];
		for ($i = 1; $i < $count_noc; $i++) {
			$raw = $rawnoc[$i];
			$arr = explode(';', $raw);
			if (!isset($customer_code)) {
				$customer_code = $arr[2];
			}
			$jjg = 0;
			for ($j = 16; $j < count($arr); $j += 2) {
				$jjg += (float)$arr[$j];
			}

			$noc[] = [
				'nocvalue' => $raw,
				'nocsite' => $arr[2],
				'nocdate' => $arr[3],
				'harvestdate' => $arr[10],
				'nocafd' => $arr[6],
				'nocblock' => $arr[7],
				'tgl_panen' => $arr[11],
				'jjg' => $jjg,
			];
		}

		$customerModel = new MCustomerModel();
		if (isset($customer_code) && (($cust = $customerModel->find($customer_code)) == null)) {
			$cust = [
				'customercode' => $customer_code,
				'name' => '[New Record - ' . $customer_code . ']',
				'active' => 'Y',
			];
			$customerModel->insert($cust);
		}
		$customer = [
			'value' => $customer_code ?? null,
			'text' => $cust['name'] ?? null,
		];

		$produk = static::produkTBS;

		$companyCode = ParameterValueModel::getValue('COMPANYCODE');
		if (isset($customer_code) && ($companyCode == $customer_code)) {
			$transaksi = static::transaksiTBSInternal;
		} else {
			//$transaksi = static::transaksiTBSExternal;
			$transaksi = static::transaksiTBSTitipOlah;
		}

		if ($count_noc > 0) {
			$siteCode = $noc[0]['nocsite'];
		} else {
			$siteCode = $companyCode;
		}
		
		$mSiteModel = new MSiteModel();
		if (($site = $mSiteModel->find($siteCode)) == null) {
			$site = [
				'sitecode' => $siteCode,
				'description' => '[New Site - '. $siteCode .']',
				'order_no' => ($mSiteModel->builder()->selectMax('order_no')->get()->getFirstRow()->order_no) + 1,
			];
			$mSiteModel->insert($site);
		}
		$site = [
			'value' => $siteCode,
			'text' => $site['description'],
		];

		$kabcode = $data[5];
		$trWbModel = new TrWbModel();
		$tr_kab = $trWbModel->where(['kabcode' => $kabcode])->first();

		$bklReturn = [
			'tipe' => 'KAB',
			'kab_type' => count($data) > 8 ? 'BOARDING' : 'NON-BOARDING',
			'sitecode' => $site,
			'customer' => $customer,
			'produk' => $produk,
			'transaksi' => $transaksi,
			'noc' => $noc,
			//'raw_noc' => $rawnoc,
			//'kab' => $data,
			'kabcode' => $kabcode,
			'chitnumber' => $tr_kab['chitnumber'] ?? '',
			'kabraw' => $rawdata,
		];

		$kabKolom3 = explode(',', $data[2]);
		if (count($kabKolom3) >= 3) {
			$unitCode = $kabKolom3[2];
			$unitTransporter = $this->getUnitTransporter($unitCode);
			$bklReturn = array_merge($bklReturn, $unitTransporter);
		}
		if (count($kabKolom3) >= 4) {
			$driverID = $kabKolom3[3];
			$driverName = $this->getEmployee($driverID)['name'];
			$bklReturn['driver'] = [
				'value' => $driverID,
				'text' => $driverName,
			];
		}
		if (count($kabKolom3) >= 5) {
			$helper1ID = $kabKolom3[4];
			if ($helper1ID != '') {
				$helper1Name = $this->getEmployee($helper1ID)['name'];
				$bklReturn['helper1'] = [
					'value' => $helper1ID,
					'text' => $helper1Name,
				];
			}
		}
		if (count($kabKolom3) >= 6) {
			$helper2ID = $kabKolom3[5];
			if ($helper2ID != '') {
				$helper2Name = $this->getEmployee($helper2ID)['name'];
				$bklReturn['helper2'] = [
					'value' => $helper2ID,
					'text' => $helper2Name,
				];
			}
		}

		return $bklReturn;
	}

	protected function getKABExternal($data, $rawdata)
	{
		$trWbModel = new TrWbModel();
		//$tr_wb = $trWbModel->find($data[3]);

		$tr_wb = $trWbModel->where(['nomorticket' => $data[3]])->first();

		$bklReturn = [
			'tipe' => 'KAB External',
			'produk' => static::produkTBS,
			'transaksi' => static::transaksiTBSExternal,
			'nomorticket' => $data[3] ?? '',
			'gate_in' => $data[4] ?? '',
			'gate_out' => $data[5] ?? '',
			'boarding_in' => $data[6] ?? '',
			'jenis_unit' => $data[7] ?? '',
			'nomor_polisi' => $data[8] ?? '',
			'nama_driver' => $data[9] ?? '',
			'kode_supplier' => $data[10] ?? '',
			'nama_supplier' => $data[11] ?? '',
			'wilayah_asal_tbs' => $data[12] ?? '',
			'kab_prop' => $data[13] ?? '',
			'kab_createdate' => $data[14] ?? '',
			'kab_createby' => $data[15] ?? '',
			'kabraw' => $rawdata,

			'chitnumber' => $tr_wb['chitnumber'] ?? '',
		];
		return $bklReturn;
	}

	/**
	 * Extract to file dan ambil text NOC
	 */
	protected function getRawNOC($noc_decoded)
	{
		$rawnoc = [];
		$tmp_folder = sys_get_temp_dir() . '/wbs';
		if (!file_exists($tmp_folder)) {
			mkdir($tmp_folder);
		}

		$file_name = tempnam($tmp_folder, 'noc');
		$file_name_extract = $file_name . '_ext';

		$fs = fopen($file_name, 'wb');
		fwrite($fs, $noc_decoded);
		fclose($fs);

		$zip = new ZipArchive();
		$x = $zip->open($file_name);
		if ($x === true) {
			$zip->extractTo($file_name_extract);
			$zip->close();

			$files = array_diff(scandir($file_name_extract), array('.', '..'));
			foreach ($files as $key => $file_dibuka) {
				$fs = fopen($file_name_extract . '/' . $file_dibuka, 'rb');
				while ($read = fgets($fs, 2400)) {
					$rawnoc[] = $read;
				}
				fclose($fs);
			}
			$this->rrmdir($file_name_extract);
		}
		unlink($file_name);

		return $rawnoc;
	}

	/*
	public function noc(){
		$raw_noc = [
			"1912031014,0111831",
			"54:A6:8D:25;;SDI1;0312191006;1;1;OB;020;K3016;V2AGLMB682004138;;031219;04B3;210B3;;379B3;16;119B3;12;253B3;15;358B3;25;383B3;4;55B3;3;386B3;2",
			"F1:DC:9A:05;;SDI1;0312191005;1;1;OB;017;K3026;V2AGLMB682005831;;031219;279B3;360B3;;53B3;11;42B3;12;334B3;7;122B3;5;22B3;9;24B3;3;11B3;2",
			"54:C4:69:DB;;SDI1;0312190859;1;1;OB;017;K3026;V2AGLMB682005831;;031219;279B3;360B3;;11B3;14;53B3;5;22B3;6;42B3;4;24B3;9;122B3;7;334B3;6",
			"84:2A:DB:25;;SDI1;0312190955;1;1;OB;014;K3014;V2AGLMC6C2406113;;031219;287B3;374B3;;111B3;4;143B3;9;19B3;9;354B3;8;284B3;4;280B3;2;319B3;2;281B3;2",
			"A0:A7:14:11;;SDI1;0312190851;1;1;OB;014;K3028;V2AGLMB682000791;;031219;01B3;337B3;;354B3;15;143B3;15;281B3;9;280B3;21;111B3;11",
			"54:F5:90:25;;SDI1;0312190902;1;1;OB;017;K3016;V2AGLMB682004138;;031219;04B3;210B3;;358B3;26;253B3;3;379B3;13;55B3;2;386B3;3;119B3;10",
			"EA:8D:93:27;;SDI1;0312190907;1;1;OB;014;K3014;V2AGLMC6C2406113;;031219;287B3;374B3;;284B3;26;111B3;7;319B3;10;280B3;6;281B3;7;354B3;10;143B3;6;19B3;11",
			"50:29:0E:11;;SDI1;0312190935;1;1;OB;017;K3028;V2AGLMB682000791;;031219;01B3;337B3;;281B3;1;19B3;3;280B3;11;383B3;13;143B3;12;386B3;4;358B3;5;354B3;3;319B3;5;55B3;1;253B3;9"
		];

		$count_noc = count($raw_noc);

		$noc = [];
		for ($i=1; $i < $count_noc; $i++) { 
			$raw = $raw_noc[$i];
			$arr = explode(';', $raw);
			if (!isset($customer_code)) {
				$customer_code = $arr[2];
			}
			$jjg = 0;
			for ($j = 16; $j < count($arr); $j += 2) {
				$jjg += (float)$arr[$j];
			}

			$noc[] = [
				'nocsite' => $arr[2],
				'nocdate' => $arr[3],
				'harvestdate' => $arr[10],
				'nocafd' => $arr[6],
				'nocblock' => $arr[7],
				'jjg' => $jjg,
				'arr' => $arr,
				'raw' => $raw,
			];
		}

		/*
		INSERT INTO TR_KAB(	CHITNUMBER, SABNO, NOCVALUE, NOCSITE, 			NOCDATE,			HARVESTDATE,		NOCAFD,				NOCBLOCK, 			JJG)
		VALUES (			CHITNUMBER, SABNO, NOCVALUE, NOCVALUE.KOLOM3,	NOCVALUE.kolom4,	NOCVALUE.kolom11,	NOCVALUE.kolom7,	NOCVALUE.kolom8,	NOCVALUE.kolom17+19+21+..)
		*

		return $this->response->setJSON([
			'customer_id' => $customer_code,
			'noc' => $noc,
			'raw_noc' => $raw_noc,
		]);
	}*/

	function rrmdir($dir)
	{
		if (is_dir($dir)) {
			$objects = scandir($dir);
			foreach ($objects as $object) {
				if ($object != "." && $object != "..") {
					if (is_dir($dir . DIRECTORY_SEPARATOR . $object) && !is_link($dir . "/" . $object))
						$this->rrmdir($dir . DIRECTORY_SEPARATOR . $object);
					else
						unlink($dir . DIRECTORY_SEPARATOR . $object);
				}
			}
			rmdir($dir);
		}
	}

	public function writeKab($id)
	{
		$trWbModel = new TrWbModel();
		if (($trWb = $trWbModel->select([
			'chitnumber',
			'transactiontype',
			'kab_type',
			'productcode',
			'kabraw',
			'wb_in',
			'weight_in',
			'wb_out',
			'weight_out',
		])->find($id)) == null) {
			return $this->response->setJSON([
				'status' => 'fail',
				'messages' => 'Chitnumber not found',
			]);
		} else if ($trWb['productcode'] != static::produkTBS['value']) {
			$namaProdukTbs = static::produkTBS['text'];
			return $this->response->setJSON([
				'status' => 'fail',
				'messages' => "Product Code is not '$namaProdukTbs'",
			]);
		}

		if ($trWb['transactiontype'] == static::transaksiTBSExternal['value']) {
			$trWb['kab_type'] = 'NON-BOARDING';
		}

		if ($trWb['kab_type'] == 'BOARDING' || $trWb['kab_type'] == 'NON-BOARDING') {

			$device = ParameterValueModel::getValue('READERCONFIG');
			$flowCtl = 'min 1 time 5 ignbrk -brkint -icrnl -imaxbel -opost -onlcr -isig -icanon -iexten -echo -echoe -echok -echoctl -echoke';
			exec('stty -F ' . $device . ' ' . $flowCtl);
			$fd = fopen($device, 'r+');
			$welcome = fgets($fd);

			if ($trWb['kab_type'] == 'NON-BOARDING') {
				$command = 'FORMATKAB';
				$hasil2 = fwrite($fd, $command);

				if (($confirm = fgets($fd)) == "OK\r\n") {
					$afterFormatMessage[] = fgets($fd, 2400);
					$afterFormatMessage[] = fgets($fd, 2400);
					$afterFormatMessage[] = fgets($fd, 2400);
					fwrite($fd, 'STOP');
					$afterFormatMessage[] = fgets($fd, 2400);
					//$confirmStop = fgets($fd, 2400);

					$bklReturn = [
						'status' => 'success',
						'messages' => 'Format KAB success',
						'respons' => $afterFormatMessage,
					];
				} else {
					$bklReturn = [
						'status' => 'fail',
						'messages' => 'Format KAB failed',
					];
				}
			} else if ($trWb['kab_type'] == 'BOARDING') {
				$command = 'WRITEID';
				$hasil2 = fwrite($fd, $command);

				if (($confirm = fgets($fd)) == "Masukkan data:\r\n") {
					$kabraw = $trWb['kabraw'];
					$sourceArray = explode(';', $kabraw);
					$writeArray = [
						0,
						0,
						0,
						$sourceArray[5],
						4,
						$sourceArray[7],
						//$trWb['wb_in'] . ' ' . $trWb['weight_in'],
						date('dmYHis', strtotime($trWb['wb_in'])). ',' . $trWb['weight_in'],
						//$trWb['wb_out'] . ' ' . $trWb['weight_out'],
						date('dmYHis', strtotime($trWb['wb_out'])). ',' . $trWb['weight_out'],
					];
					$writeRaw = implode(';', $writeArray);
					sleep(1);
					$hasil3 = fwrite($fd, $writeRaw . "\r\n");
					$afterFormatMessage[] = fgets($fd, 2400);
					$afterFormatMessage[] = fgets($fd, 2400);
					$afterFormatMessage[] = fgets($fd, 2400);
					$afterFormatMessage[] = fgets($fd, 2400);

					//$afterFormatMessage[] = $writeRaw;
					//sleep(5);
					$hasil4 = fwrite($fd, 'STOP');
					$afterFormatMessage[] = fgets($fd, 2400);
					//$afterFormatMessage[] = fgets($fd, 2400);

					$bklReturn = [
						'status' => 'success',
						'messages' => 'Write KAB success',
						'respons' => $afterFormatMessage,
					];
				} else {
					$bklReturn = [
						'status' => 'fail',
						'messages' => 'Write KAB failed',
						'confirm' => $confirm,
					];
				}
			}

			fclose($fd);

			return $this->response->setJSON($bklReturn);
		} else {
			return $this->response->setJSON([
				'status' => 'fail',
				'messages' => 'Type KAB tidak terdefinisi',
			]);
		}
	}

	public function weighRead()
	{
		//$device = '/dev/ttyACM0';
		$device = ParameterValueModel::getValue('WBPORTCONFIG');
		//$baudrate = '2400';
		$baudrate = ParameterValueModel::getValue('WBBAUDRATE');
		//$parity = 'even';
		$parity = ParameterValueModel::getValue('WBPARITY');
		//$datalengt = '7';
		$datalengt = ParameterValueModel::getValue('WBDATALEN');
		//$stopbits = '2';
		$stopbits = ParameterValueModel::getValue('WBSTOPBITS');
		//$rawformat = 'ST,GS,+XXXXXXXkg';
		$rawformat = ParameterValueModel::getValue('WBOUTPUTCONFIG');

		$bakalReturn = [
			'device' => $device,
			'baudrate' => $baudrate,
			'parity' => $parity,
			'datalengt' => $datalengt,
			'stopbits' => $stopbits,
			'raw-format' => $rawformat,
		];

		$wb_setting = [
			'parity' => [
				'none' => '-parenb',
				'even' => '-parodd',
				'odd' => 'parodd',
			],
			'data' => [
				'5' => 'cs5',
				'6' => 'cs6',
				'7' => 'cs7',
				'8' => 'cs8',
			],
			'stopbits' => [
				'1' => '-cstopb',
				'2' => 'cstopb',
			],
		];
		$parity = $wb_setting['parity'][$parity] ?? '';
		$data = $wb_setting['data'][$datalengt] ?? '';
		$stopbits = $wb_setting['stopbits'][$stopbits] ?? '';

		if (!file_exists($device)) {
			$bakalReturn['value'] = '#Err';
			$bakalReturn['error'] = "'$device' not exist";
		} else {
			$flowCtl = 'min 1 time 5 ignbrk -brkint -icrnl -imaxbel -opost -onlcr -isig -icanon -iexten -echo -echoe -echok -echoctl -echoke';
			exec('stty '. $baudrate .' -F ' . $device . ' '. $parity . ' ' . $data . ' ' . $stopbits . ' ' . $flowCtl);
	
			$fd = fopen($device, 'rb');
			$rawBerat = fgets($fd);
			fclose($fd);

			$berat = trim($rawBerat);
			$bakalReturn['raw'] = $berat;

			/*
			$berat = explode(' ', $berat);
			$berat = number_format($berat[7], 0, ',', '.');
			$bakalReturn['value'] = $berat;
			*/
			$lenFormat = strlen($rawformat);
			if (strlen($berat) == $lenFormat) {
				$value = '0';
				for ($i=0; $i < $lenFormat; $i++) { 
					$k = substr($rawformat, $i, 1);
					if (($k == 'X') || ($k == 'x')) {
						$value .= substr($berat, $i, 1);
					} else {
						if (substr($berat, $i, 1) != substr($rawformat, $i, 1)) {
							$bakalReturn['error'] = "Format in position '$i' missmatch.";
							$bakalReturn['value'] = 0;
							return $this->response->setJSON($bakalReturn);
						}
					}
					$value = intval($value);
					$bakalReturn['value'] = number_format($value, 0, ',', '.');
				}
			} else {
				$bakalReturn['error'] = 'Data length missmatch.';
				$bakalReturn['value'] = 0;
			}
		}

		return $this->response->setJSON($bakalReturn);
		//die;

		//return $berat;
		/*
		if ((strlen($berat) == 16) && (substr($berat, 0, 7) == 'ST,GS,+')) {
			$berat = str_replace('ST,GS,+', '', $berat);
			$berat = str_replace('kg', '', $berat);
			$berat = intval($berat);
			return number_format($berat, 0, ',', '.');	
		} else {
			return "0";
		}
		*/
	}

	public function index()
	{
		$mSiteModel = new MSiteModel();
		$mSites = $mSiteModel->orderBy('description', 'asc')
			->findAll();

		$customerModel = new MCustomerModel();
		$customers = $customerModel->where(['deleted_at' => null])
			->orderBy('name', 'asc')
			->where('active', 'Y')
			->findAll();

		$transporterModel = new MTransporterModel();
		$transporters = $transporterModel->where(['deleted_at' => null])
			->orderBy('name', 'asc')
			->where('active', 'Y')
			->findAll();

		$unitModel = new MUnitModel();
		$units = $unitModel->where(['deleted_at' => null])
			->orderBy('unitcode', 'asc')
			->where('active', 'Y')
			->findAll();

		$parameterValueModel = new ParameterValueModel($this->request);
		$parameter_values = $parameterValueModel->where('active', 'Y')
			->orderBy('description', 'asc')
			->findAll();
		$parameterValuesMap = [];
		$companyCode = '';
		foreach ($parameter_values as $value) {
			if (!isset($parameterValuesMap[$value['parameter_code']])) {
				$parameterValuesMap[$value['parameter_code']] = [
					$value['value'] => $value['description']
				];
			} else {
				$parameterValuesMap[$value['parameter_code']][$value['value']] = $value['description'];
			}
			if ($value['parameter_code'] == 'COMPANYCODE') {
				$companyCode = $value['value'];
			}
		}

		$employeeModel = new MEmployeeModel();
		$empleyees = $employeeModel->where(['deleted_at' => null])
			->where('active', 'Y')
			->orderBy('name', 'asc')
			->findAll();

		/** Cari Tr WB untuk edit pending transaction */
		if (($chitnumber = $this->request->getGet('id')) != null) {
			$trWbModel = new TrWbModel();
			$tr_wb = $trWbModel
				->join('m_unit', 'm_unit.unitcode = tr_wb.unitcode', 'left')
				->select([
					'tr_wb.*',
					'm_unit.transportercode',
				])
				->find($chitnumber);
			$tr_wb['netto'] = abs(($tr_wb['weight_out'] ?? 0) - ($tr_wb['weight_in'] ?? 0));

			$trKabModel = new TrKabModel();
			$tr_kab = $trKabModel->where('chitnumber', $chitnumber)
				->findAll();
			/*
			$db = Database::connect();
			$lastQuery = $db->showLastQuery();
			return $lastQuery; die;
			*/

			$trGradingModel = new TrGradingModel();
			$tr_grading = $trGradingModel->find($chitnumber);

			$trQualityModel = new TrCpoKernelQualityModel();
			$tr_quality = $trQualityModel->find($chitnumber);
		} else {
			$tr_wb = [
				'wbsitecode' => $companyCode,
				'sitecode' => $companyCode,
			];
		}

		$preset = [
			'sites' => $mSites,
			'customers' => $customers,
			'transporters' => $transporters,
			'units' => $units,
			'parameter_values' => $parameterValuesMap,
			'employees' => $empleyees,

			'tr_wb' => $tr_wb,
			'tr_kab' => $tr_kab ?? [],
			'tr_grading' => $tr_grading ?? [],
			'tr_quality' => $tr_quality ?? [],
		];

		//return $this->response->setJSON($preset);die;

		return view('transaksi/timbang/index', $preset);
	}

	private function generateNewChitNumber()
	{
		$prefiks = 'SELECT DATE_FORMAT(NOW(), (SELECT `value` FROM m_parameter_values WHERE parameter_code = "CHITNUMBERCONFIG")) prefiks';
		$db = Database::connect();
		$prefiks = $db->query($prefiks)->getRow()->prefiks;
		$len = strlen($prefiks);
		$terakhir = "SELECT MAX(CAST(TRIM(RIGHT(`chitnumber`, LENGTH(`chitnumber`) - $len)) AS SIGNED)) cht FROM tr_wb WHERE `chitnumber` LIKE '$prefiks%'";
		$terakhir = $db->query($terakhir)->getRow()->cht ?? 0;
		$terakhir++;
		return $prefiks . sprintf('%03d', $terakhir);
	}

	public function save()
	{
		/** Validasi Awal */
		if (! $this->validate([
			'productcode' => 'required',
			'transactiontype' => 'required',
		])) {
			return $this->returnErrorInvalidate();
		}

		$post = $this->request->getPost();

		/** Transaksi TBS External harus ada customercode */
		if ($post['transactiontype'] == static::transaksiTBSExternal['value']) {
			/*if (!$this->validate([
				//'nomorticket' => 'required',
				'customercode' => 'required',
			])) {
				return $this->returnErrorInvalidate();
			}*/
		} else {
			if (!$this->validate([
				'customercode' => 'required',
				'transportercode' => 'required',
				'unitcode' => 'required',
				'npk_driver' => 'required',
			])) {
				return $this->returnErrorInvalidate();
			}

			/** Jika Produk TBS, dan bukan TBS External */
			if ($post['productcode'] == static::produkTBS['value']) {
				/** Cek: TBS harus ada noc/kab detail */
				/*if (!$this->validate([
					'noc' => [
						'rules' => 'required',
						'errors' => [
							'required' => 'Data KAB / NOC belum di tap'
						]
					]
				])) {
					return $this->returnErrorInvalidate();
				}*/
			}
		}

		$tambahBaru = false;
		/** Cek: tambah baru atau edit data */
		if (!isset($post['chitnumber']) || $post['chitnumber'] == '') {
			/** Set new chitnumber */
			$post['chitnumber'] = $this->generateNewChitNumber();

			/** mark tambah baru */
			$tambahBaru = true;

			if (key_exists('nomorticket', $post) && !key_exists('kabcode', $post)) {
				$post['kabcode'] = $post['nomorticket'];
			}
		}

		/** Set: sabno otomatis, jika kosong, untuk ProdukTBS dan bukan TransaksiTBSExternal */
		if ((!isset($post['sabno']) || $post['sabno'] == '') 
		&& ($post['productcode'] == static::produkTBS['value'])
		&& ($post['transactiontype'] != static::transaksiTBSExternal['value'])) {

			$prefiks = 'SELECT DATE_FORMAT(NOW(), (SELECT `value` FROM m_parameter_values WHERE parameter_code = "SABNOCONFIG")) prefiks';
			$db = Database::connect();
			$prefiks = $db->query($prefiks)->getRow()->prefiks;

			$len = strlen($prefiks);
			$terakhir = "SELECT MAX(CAST(TRIM(RIGHT(`sabno`, LENGTH(`sabno`) - $len)) as SIGNED)) sbn FROM tr_kab WHERE `sabno` LIKE '$prefiks%'";
			$terakhir = $db->query($terakhir)->getRow()->sbn ?? 0;
			$terakhir++;

			$sabno = $prefiks . sprintf('%03d', $terakhir);

			$post['sabno'] = $sabno;

		}

		/** Set: Clean dot (.) di weight_in dan weight_out */
		if (isset($post['weight_in']) && $post['weight_in'] != '') {
			$post['weight_in'] = str_replace('.', '', $post['weight_in']);
		}
		if (isset($post['weight_out']) && $post['weight_out'] != '') {
			$post['weight_out'] = str_replace('.', '', $post['weight_out']);
		}
	
		/** Set: status */
		if ((isset($post['weight_out']) && $post['weight_out'] != '' && intval($post['weight_out']) > 0)
		&& (isset($post['weight_in']) && $post['weight_in'] != '' && intval($post['weight_in']) > 0)
		) {
			$post['status'] = 1; // set status complete				
		} else {
			$post['status'] = 0; // set status pending
		}
	
		/** Set: Chitbumber dan SabNo di noc / kab detail */
		if (isset($post['noc'])) {
			foreach ($post['noc'] as $key => &$noc) {
				$noc['chitnumber'] = $post['chitnumber'];
				$noc['sabno'] = $post['sabno'] ?? '';
			}
		}
		
		/** Simpan Data */
		$trWbModel = new TrWbModel();					
		if ($tambahBaru) {
			$post['operator_in'] = session()->get('name');
			$post['created_at'] = date('Y-m-d');
			$trWbModel->insert($post);
			$post['sent'] = 0;
		} else {
			$post['operator_out'] = session()->get('name');
			$post['updated_at'] = date('Y-m-d');
			$trWbModel->update($post['chitnumber'], $post);
			$saved = $trWbModel->find($post['chitnumber']);
			$post['sent'] = $saved['sent'];
		}

		/** Simpan NOC / Detail KAB jika ada */
		if (isset($post['noc'])) {
			$trKabModel = new TrKabModel();
			if ($tambahBaru) {
				$trKabModel->insertBatch($post['noc']);
			} else {
				foreach ($post['noc'] as $kab) {
					$trKabModel->save($kab);
				}
			}
		}

		$this->simpanGrading($post);
		$this->simpanCpoKernelQuality($post);
		$this->updateUnitCode($post);

		/** Response */
		return $this->response->setJSON([
			'status' => 'success',
			'new' => $tambahBaru,
			'messages' => 'Data timbang berhasil di ' . ($tambahBaru ? 'simpan' : 'ubah'), // . ' API: ' .  ($api_response['messages'] ?? ''),
			'chitnumber' => $post['chitnumber'],
			'post' => $post,
			'saveAPI' => (($post['status'] == 1) && ($post['sent'] != 'Y')),
		]);		
	}

	public function saveAPI($chitnumber)
	{
		$cron = new Cron;
		return $this->response->setJSON($cron->scheduler($chitnumber));
	}

	private function returnErrorInvalidate()
	{
		$errors = $this->validator->getErrors();
		$inputErrors = implode(', ', array_keys($errors));
		return $this->response->setJSON([
			'status' => 'invalid',
			'messages' => "Terdapat input yang tidak valid.\n [$inputErrors]",
			'error' => $errors,
		]);
	}

	private function simpanGrading(array $post)
	{
		if ((($post['grading']['jumlahsampling']) ?? '') != '') {
			$chitnumber = $post['chitnumber'];
			$model = new TrGradingModel();
			$grading = $post['grading'];
			$grading['chitnumber'] = $chitnumber;
			if ($model->find($chitnumber) == null) {
				$model->insert($grading);
			} else {
				$model->update($chitnumber, $grading);
			}
		}
	}

	private function simpanCpoKernelQuality(array $post)
	{
		if (((($post['kualitas']['ffa']) ?? '') != '') && ((($post['kualitas']['temperature']) ?? '') != '')) {
			$chitnumber = $post['chitnumber'];
			$kualitas = $post['kualitas'];
			$model = new TrCpoKernelQualityModel();
			$kualitas['chitnumber'] = $chitnumber;
			if ($model->find($chitnumber) == null) {
				$model->insert($kualitas);
			} else {
				$model->update($chitnumber, $kualitas);
			}
		}
	}

	private function updateUnitCode(array $post)
	{
		if (key_exists('unitcode', $post)) {
			$unitCodeModel = new MUnitModel();
			if (($unit = $unitCodeModel->find($post['unitcode'])) != null) {
				if ($unit['transportercode'] == null) {
					$unit['transportercode'] = $post['transportercode'];
				}
				$unitCodeModel->update($post['unitcode'], $unit);
				return true;
			}
		}
	}

	public function getTransaction($productCode)
	{
		$productTransMapModel = new MProductTransMapModel;
		$product_trans_map = $productTransMapModel->where("productcode = '$productCode'")->findAll();
		return $this->response->setJSON($product_trans_map);
	}
}