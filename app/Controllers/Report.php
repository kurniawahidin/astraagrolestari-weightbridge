<?php
namespace App\Controllers;

use App\Models\ServerSideModel;
use App\Models\TrWbModel;
use CodeIgniter\Database\Config;
use Config\Database;
use Config\Services;

class Report extends BaseController
{
	private $serverside_model;

	public function __construct()
	{
		$this->serverside_model = new ServerSideModel;
		return $this;
	}

	public function allcolumn()
	{
		$model = new TrWbModel();
		if ($this->request->getMethod() == 'post') {
			return $model->makeDataTable();
		}
		$listColumns = [
			'wbsitecode',
			'sitecode',
			'chitnumber',
			'sabno',
			'customercode',
			'productcode',
			'transactiontype',
			'unitcode',
			'npk_driver',
			'npk_helper1',
			'npk_helper2',
			'boarding',
			'wb_in',
			'wb_out',
			'weight_in',
			'weight_out',
			'kab_type',
			'kabcode',
			'kabraw',
			'operator_in',
			'terminal_in',
			'terminal_out',
			'operator_out',
			'nomorticket',
			'gate_in',
			'gate_out',
			'boarding_in',
			'jenis_unit',
			'nomor_polisi',
			'nama_driver',
			'kode_supplier',
			'wilayah_asal_tbs',
			'nama_supplier',
			'status',
			'sent',
		];
		return view('report/allcolumn', [
			'model' => $model,
			'request' => $this->request,
			'listColumns' => $listColumns,
			'detailColumns' => [],
		]);
	}

	public function produksi()
	{
		if ($this->request->getMethod() == 'post') {
			$dateFrom = $this->request->getPost('dateFrom');
			$dateTo = $this->request->getPost('dateTo') . ' 23:59:59';
			$afdeling = $this->request->getPost('afdeling');

			$db = Database::connect();
			$tabel = $db->table('tr_kab')
				->join('tr_wb', 'tr_wb.chitnumber = tr_kab.chitnumber')
				->select([
					'tr_wb.wb_in AS tanggal',
					'tr_wb.chitnumber',
					'tr_kab.nocafd AS afdeling',
					'tr_kab.nocblock AS block',
					'SUM(tr_kab.jjg) AS janjang',
				])
				->groupBy([
					'tr_wb.wb_in',
					'tr_wb.chitnumber',
					'tr_kab.nocafd',
					'tr_kab.nocblock',
				])
				->where("tr_wb.wb_in BETWEEN '$dateFrom' AND '$dateTo'");
			if ($afdeling != '') {
				$tabel->where('tr_kab.nocafd', $afdeling);
			}

			$count_all = $tabel->countAllResults(false);
			//$count_all_query = $db->showLastQuery();
			
			if ($_POST['search']['value']) {
				$searchValue = $_POST['search']['value'];
				$tabel->groupStart();
				$tabel->like('tr_wb.chitnumber', $searchValue);
				$tabel->orLike('tr_kab.nocafd', $searchValue);
				$tabel->orLike('tr_kab.nocblock', $searchValue);
				$tabel->groupEnd();
			}

			$count_filtered = $tabel->countAllResults(false);
			$sum_janjang = $db->table('(' . $tabel->getCompiledSelect(false) . ') AS src')
				->selectSum('janjang', 'sum_jjg')->get()->getFirstRow()->sum_jjg;
			//$count_filtered_query = $db->showLastQuery();

			if ($_POST['length'] != -1) $tabel->limit($_POST['length'], $_POST['start']);

			$data = $tabel->get()->getResult();
			//$data_query = $db->showLastQuery();

			return json_encode([
				'draw' => $this->request->getPost('draw'),
				'recordsTotal' => $count_all,
				'recordsFiltered' => $count_filtered,
				'sum_janjang' => $sum_janjang,
				'data' => $data,
				//'data_query' => $data_query,
				//'count_all_query' => $count_all_query,
				//'count_filtered_query' => $count_filtered_query,
			]);
		}
		return view('report/produksi');
	}
}